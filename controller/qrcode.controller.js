const qr = require('qrcode');

exports.qrcode = async (req, res, next) => {
    const { param } = req.body;

    if (param.length === 0) res.send('Empty Data');

    qr.toDataURL(param, (err, src) => {
        if (err) res.send('Error occured');

        res.send('<img src="' + src + '" />');
    });
};

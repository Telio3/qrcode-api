const router = require('express').Router();
const qrcode = require('./qrcode.routes');

router.use('/qrcode', qrcode);

router.get('/', (req, res) => {
    res.send('QRCODE API');
});

module.exports = router;

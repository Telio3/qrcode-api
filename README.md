## QrCode API

***

Cette API permet de générer un QrCode à partir d'un paramètre passé dans la requête.

Il est compatible avec tout type de contenu.

Une documentation est fournie une fois le serveur lancé sur la route :

```
/api-docs
```

***

#### Exemple d'utilisation

**POST** sur la route :
```
/qrcode
```
Accompagné du **JSON** suivant :
```json
{
    "param": "https://gitlab.com"
}
```
Retour de l'**API** :

![QrCode](https://www.zupimages.net/up/22/12/jkey.png)

Le **QrCode** a été généré !

### Lancer le Projet

***

```
npm i          (installer les dépendances)
```
```
npm start          (lancer le serveur)
```

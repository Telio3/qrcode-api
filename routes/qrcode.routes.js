const router = require('express').Router();
const { qrcode } = require('../controller/qrcode.controller');

router.post('/', qrcode);

module.exports = router;

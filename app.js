const express = require('express');
const bp = require('body-parser');
const routes = require('./routes');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./specs/specs.yaml');

const app = express();

exports.app = app;

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(bp.urlencoded({ extended: false }));
app.use(bp.json());
app.use(routes);
